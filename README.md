# Установка Ubuntu

Скрипт создает ссылки на зеркала ДокерХаба по всему миру
1. Клонируем репозиторий
2. Запускаем в баше ./docker_proxy.sh

И пусть удавятся, жадины :)

# Пояснения для других ОС

Эти прокси можно использовать, если вы получаете ошибку
Error response from daemon: pull access denied for nginx, repository does not exist or may require 'docker login': denied: <html><body><h1>403 Forbidden</h1> Since Docker is a US company, we must comply with US export control regulations. In an effort to comply with these, we now block all IP addresses that are located in Cuba, Iran, North Korea, Republic of Crimea, Sudan, and Syria. If you are not in one of these cities, countries, or regions and are blocked, please reach out to https://hub.docker.com/support/contact/ </body></html>


Как подключить
1. Через конфиг докера (как зеркало docker.io)

Конфиг расположен в:
- Linux, обычная установка	/etc/docker/daemon.json
- Linux, режим rootless	~/.config/docker/daemon.json
- Windows	C:\ProgramData\docker\config\daemon.json
- Windows с Docker Desktop	C:\Users\<Пользователь>\.docker\daemon.json

Конфиг:
```bash
{ 
  "registry-mirrors": [ 
    "https://dockerhub1.beget.com",
    "https://mirror.gcr.io",
    "https://dockerhub.timeweb.cloud" 
  ] 
}
```

Чтобы конфиг применился, потребуется перезапустить конфигурацию:
sudo systemctl reload docker

Теперь при попытке загрузки образа, Docker будет сначала пытаться использовать прокси.

2. Явное указание адреса
Например:

`docker pull dockerhub.timeweb.cloud/library/alpine:latest`
или
`docker pull dockerhub.timeweb.cloud/openresty/openresty:latest`